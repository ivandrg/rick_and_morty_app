# rick_and_morty_app

Aplicacion de la serie rick y morty hecha en flutter, para ser ejecutado el proyecto se deben seguir los siguiente pasos:
- Clonar el repositorio de gitlab a su entorno local desde la rama master
- El proyecto esta realizado en Flutter 2.8.1 con null safety por lo tanto debe ser ejecutado con estas versiones para su correcto funcionamiento
- se deben descargar las librerias con el comando: flutter pub get
- para ejecutar el proyecto puede utilizar el comando: flutter run  

## Screenshots

<img src="assets/screenshot_1.png" width=500 >

<img src="assets/screenshot_2.png" width=500 >
