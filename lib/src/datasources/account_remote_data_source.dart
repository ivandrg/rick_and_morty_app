import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:rick_and_morty_app/src/models/character_model.dart';
import 'package:rick_and_morty_app/src/models/episodes_model.dart';

abstract class ProfileRemoteDataSource {
  initClientGraphql(BuildContext? context);
  clearCacheClientGraphql();
  Future userGetCharacters(
    int page,
  );
  Future getEpisodes(
    int page,
  );
}

class ProfileRemoteDataSourceImpl implements ProfileRemoteDataSource {
  GraphQLClient? client;
  QueryResult? dataResponse;
  QueryResult? dataRefetch;
  QueryOptions? dataQueryOptions;
  CharacterModel? queryCharactertModel;
  CharacterModel? queryRefetchtModel;
  List<ResultCharacter> listCurrentData = [];
  List<ResultCharacter> listRefetch = [];

  ProfileRemoteDataSourceImpl({
    this.client,
    this.dataResponse,
    this.dataRefetch,
    this.dataQueryOptions,
    this.queryCharactertModel,
    this.queryRefetchtModel,
    required this.listCurrentData,
    required this.listRefetch,
  });
  @override
  Future userGetCharacters(int page) async {
    return await userGetCharactersFromApi(page);
  }

  Future userGetCharactersFromApi(int page) async {
    // if (client == null) initClientGraphql(context);
    if (true) await clearCacheClientGraphql();

    const String getCharacters = """
                        query getCharacters(\$page: Int){
                          characters(page: \$page) {
                            info {
                              count
                            }
                            results {
                              name
                              status
                              species
                              type
                              gender
                              origin {
                                name
                                type
                                dimension
                              }
                              image
                              location{
                                name
                                type
                              }
                              episode{
                                name
                                episode
                              }
                            }
                          }
                        }
                      """;

    dataQueryOptions = QueryOptions(
      document: gql(getCharacters),
      variables: {
        "page": page,
      },
    );
    if (page > 1) {
      dataRefetch = await client!.query(dataQueryOptions!);
      queryRefetchtModel = CharacterModel.fromJson(dataRefetch!.data!);
      listRefetch = queryRefetchtModel!.data.characters.results;
    }

    try {
      //! No Internet Connection
      // throw SocketException('No Internet');
      //! 404
      // throw const HttpException('404');
      //! Invalid JSON (throws FormatException)
      // return 'abcd';
      if (page == 1) {
        dataResponse = await client!.query(dataQueryOptions!);
        queryCharactertModel = CharacterModel.fromJson(dataResponse!.data!);
        listCurrentData = queryCharactertModel!.data.characters.results;
        return queryCharactertModel!.data.characters.results;
      } else {
        final List<ResultCharacter> repos = [
          ...listCurrentData,
          ...listRefetch
        ];
        listCurrentData = repos;
        return repos;
      }
    } on ServerException {
      throw Failure('No Internet connection 😑');
    } on SocketException {
      throw Failure('No Internet connection 😑');
    } on HttpException {
      throw Failure("Couldn't find the post 😱");
    } on FormatException {
      throw Failure("Bad response format 👎");
    } on TypeError {
      throw Failure("Graphql Error 👎");
    }

    // if (dataResponse!.hasException) {
    //   return dataResponse!.exception;
    // } else {
    //   CharacterModel queryDatapetModel =
    //       CharacterModel.fromJson(dataResponse!.data!);
    //   return queryDatapetModel.data.characters;
    // }
  }

  @override
  clearCacheClientGraphql() async {
    // ignore: unnecessary_statements
    await client!.resetStore();
  }

  @override
  initClientGraphql(BuildContext? context) {
    client = GraphQLProvider.of(context!).value;
  }

  @override
  Future getEpisodes(int page) async {
    return await getEpisodesFromApi(page);
  }

  Future getEpisodesFromApi(int page) async {
    const String getEpisodes = """
                        query getEpisodes(\$page: Int) {
                          episodes(page: \$page) {
                            info {
                              count
                            }
                            results {
                              id
                              name
                              air_date
                              episode
                              created
                            }
                          }
                        }
                      """;

    dataQueryOptions = QueryOptions(
      document: gql(getEpisodes),
      variables: {
        "page": 1,
      },
    );
    // dataResponse = await client!.query(dataQueryOptions!);

    try {
      //! No Internet Connection
      // throw SocketException('No Internet');
      //! 404
      // throw const HttpException('404');
      //! Invalid JSON (throws FormatException)
      // return 'abcd';
      dataResponse = await client!.query(dataQueryOptions!);
      EpisodesModel queryEpisodesModel =
          EpisodesModel.fromJson(dataResponse!.data!);
      return queryEpisodesModel.data.episodes.info.count;
    } on ServerException {
      throw Failure('No Internet connection 😑');
    } on SocketException {
      throw Failure('No Internet connection 😑');
    } on HttpException {
      throw Failure("Couldn't find the post 😱");
    } on FormatException {
      throw Failure("Bad response format 👎");
    } on TypeError {
      throw Failure("Graphql Error 👎");
    }

    //   if (dataResponse!.hasException) {
    //     return dataResponse!.exception;
    //   } else {
    //     EpisodesModel queryDatapetModel =
    //         EpisodesModel.fromJson(dataResponse!.data!);
    //     return queryDatapetModel.data.episodes.info.count;
    //   }
    // }
  }
}

class Failure {
  final String message;

  Failure(this.message);

  @override
  String toString() => message;
}
