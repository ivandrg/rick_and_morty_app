// To parse this JSON data, do
//
//     final characterModel = characterModelFromJson(jsonString);

import 'dart:convert';

CharacterModel characterModelFromJson(String str) =>
    CharacterModel.fromJson(json.decode(str));

String characterModelToJson(CharacterModel data) => json.encode(data.toJson());

class CharacterModel {
  CharacterModel({
    required this.data,
  });

  Data data;

  factory CharacterModel.fromJson(Map<String, dynamic> json) => CharacterModel(
        data: Data.fromJson(json),
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.characters,
  });

  CharactersMdl characters;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        characters: CharactersMdl.fromJson(json["characters"]),
      );

  Map<String, dynamic> toJson() => {
        "characters": characters.toJson(),
      };
}

class CharactersMdl {
  CharactersMdl({
    required this.info,
    required this.results,
  });

  Info info;
  List<ResultCharacter> results;

  factory CharactersMdl.fromJson(Map<String, dynamic> json) => CharactersMdl(
        info: Info.fromJson(json["info"]),
        results:
            List<ResultCharacter>.from(json["results"].map((x) => ResultCharacter.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Info {
  Info({
    required this.count,
  });

  int count;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
      };
}

class ResultCharacter {
  ResultCharacter({
    required this.name,
    this.status,
    this.species,
    this.type,
    this.gender,
    this.origin,
    this.image,
    this.location,
    this.episode,
  });

  String name;
  String? status;
  String? species;
  String? type;
  String? gender;
  Origin? origin;
  String? image;
  Location? location;
  List<Episode>? episode;

  factory ResultCharacter.fromJson(Map<String, dynamic> json) => ResultCharacter(
        name: json["name"],
        // status: statusValues.map[json["status"]],
        status: json["status"],
        species: json["species"],
        type: json["type"],
        // gender: genderValues.map[json["gender"]],
        gender: json["gender"],
        origin: Origin.fromJson(json["origin"]),
        image: json["image"],
        location: Location.fromJson(json["location"]),
        episode:
            List<Episode>.from(json["episode"].map((x) => Episode.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        // "status": statusValues.reverse[status],
        "status": status,
        "species": species,
        "type": type,
        // "gender": genderValues.reverse[gender],
        "gender": gender,
        "origin": origin!.toJson(),
        "image": image,
        "location": location!.toJson(),
        "episode": List<dynamic>.from(episode!.map((x) => x.toJson())),
      };
}

class Episode {
  Episode({
    this.name,
    this.episode,
  });

  String? name;
  String? episode;

  factory Episode.fromJson(Map<String, dynamic> json) => Episode(
        name: json["name"],
        episode: json["episode"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "episode": episode,
      };
}

class Location {
  Location({
    this.name,
    this.type,
  });

  String? name;
  String? type;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        name: json["name"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "type": type,
      };
}

// enum Gender { male, female, unknown }

// final genderValues = EnumValues(
//     {"Female": Gender.female, "Male": Gender.male, "unknown": Gender.unknown});

class Origin {
  Origin({
    required this.name,
    this.type,
    this.dimension,
  });

  String name;
  String? type;
  String? dimension;

  factory Origin.fromJson(Map<String, dynamic> json) => Origin(
        name: json["name"],
        type: json["type"],
        dimension: json["dimension"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "type": type,
        "dimension": dimension,
      };
}

// enum Status { unknown, alive, dead }

// final statusValues = EnumValues(
//     {"Alive": Status.alive, "Dead": Status.dead, "unknown": Status.unknown});

// class EnumValues<T> {
//   Map<String, T> map;
//   Map<T, String>? reverseMap;

//   EnumValues(this.map);

//   Map<T, String> get reverse {
//     reverseMap;
//     return reverseMap!;
//   }
// }
