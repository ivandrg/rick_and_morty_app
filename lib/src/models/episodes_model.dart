// To parse this JSON data, do
//
//     final episodesModel = episodesModelFromJson(jsonString);

import 'dart:convert';

EpisodesModel episodesModelFromJson(String str) =>
    EpisodesModel.fromJson(json.decode(str));

String episodesModelToJson(EpisodesModel data) => json.encode(data.toJson());

class EpisodesModel {
  EpisodesModel({
    required this.data,
  });

  Data data;

  factory EpisodesModel.fromJson(Map<String, dynamic> json) => EpisodesModel(
        data: Data.fromJson(json),
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.episodes,
  });

  Episodes episodes;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        episodes: Episodes.fromJson(json["episodes"]),
      );

  Map<String, dynamic> toJson() => {
        "episodes": episodes.toJson(),
      };
}

class Episodes {
  Episodes({
    required this.info,
    required this.results,
  });

  Info info;
  List<Result> results;

  factory Episodes.fromJson(Map<String, dynamic> json) => Episodes(
        info: Info.fromJson(json["info"]),
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Info {
  Info({
   required this.count,
  });

  int count;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        count: json["count"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
      };
}

class Result {
  Result({
    this.id,
    this.name,
    this.airDate,
    this.episode,
    this.created,
  });

  String? id;
  String? name;
  String? airDate;
  String? episode;
  DateTime? created;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        name: json["name"],
        airDate: json["air_date"],
        episode: json["episode"],
        created: DateTime.parse(json["created"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "air_date": airDate,
        "episode": episode,
        "created": created!.toIso8601String(),
      };
}
