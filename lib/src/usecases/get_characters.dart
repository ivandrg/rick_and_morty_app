
import 'package:rick_and_morty_app/src/datasources/account_remote_data_source.dart';

class GetCharactersUseCase {
  final ProfileRemoteDataSource remoteDataSource;

  GetCharactersUseCase(this.remoteDataSource);

  Future executeGetCharacters(int page) async {
    return await remoteDataSource.userGetCharacters(page);
    // return characters;
  }
}
