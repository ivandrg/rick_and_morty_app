import 'package:rick_and_morty_app/src/datasources/account_remote_data_source.dart';

class GetEpisodesUseCase {
  final ProfileRemoteDataSource remoteDataSource;

  GetEpisodesUseCase(this.remoteDataSource);

  Future executeGetEpisodes(int page) async {
    final int episodes = await remoteDataSource.getEpisodes(page);
    return episodes;
  }
}
