import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/src/models/character_model.dart';

class DetailCharactesPage extends StatelessWidget {
  const DetailCharactesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: DetailCharacterBody(),
      ),
    );
  }
}

class DetailCharacterBody extends StatelessWidget {
  const DetailCharacterBody({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    ResultCharacter character = ModalRoute.of(context)!.settings.arguments as ResultCharacter;
    final _size = MediaQuery.of(context).size;
    double _radius = 25;
    return SizedBox(
      height: _size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Center(
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(_radius),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(_radius),
                    ),
                    child: FadeInImage(
                      placeholder: const AssetImage('assets/img/loading.gif'),
                      image: NetworkImage(character.image!),
                      fit: BoxFit.cover,
                      width: _size.width * 0.90,
                      height: _size.height * 0.55,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: _size.width * 0.4,
                top: _size.height * 0.04,
                child: const Text(
                  'Detalle',
                  style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                    letterSpacing: -0,
                    color: Colors.white,
                  ),
                ),
              ),
              Positioned(
                left: _size.width * 0.1,
                top: _size.height * 0.03,
                child: Container(
                  height: 36,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    // border: Border.all(),
                  ),
                  child: Center(
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.arrow_back_ios_sharp,
                        color: Colors.grey[400],
                        size: 20,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Spacer(
                    flex: 2,
                  ),
                  Text(
                    character.name,
                    style: const TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const Spacer(
                    flex: 3,
                  ),
                  Text(
                    'Género: ${character.gender!}',
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      letterSpacing: -1,
                      color: Colors.black54,
                    ),
                  ),
                  const Spacer(
                    flex: 1,
                  ),
                  Text(
                    'Origen: ${character.origin.toString()}',
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      letterSpacing: -1,
                      color: Colors.black54,
                    ),
                  ),
                  const Spacer(
                    flex: 1,
                  ),
                  Text(
                    'Ubicación: ${character.location!.name!}',
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      letterSpacing: -1,
                      color: Colors.black54,
                    ),
                  ),
                  const Spacer(
                    flex: 1,
                  ),
                  Text(
                    'Episodios: ${character.episode!.length}',
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                      letterSpacing: -1,
                      color: Colors.black54,
                    ),
                  ),
                  const Spacer(
                    flex: 10,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
