import 'package:flutter/material.dart';
import 'package:rick_and_morty_app/src/cubit/characters_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty_app/src/models/character_model.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: const Center(
          child: Text(
            'Rick y Morty',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w600,
              letterSpacing: 0,
              fontSize: 40,
              fontFamily: 'get_schwifty',
            ),
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: const HomePageBody(),
    );
  }
}

class HomePageBody extends StatefulWidget {
  const HomePageBody({Key? key}) : super(key: key);

  @override
  _HomePageBodyState createState() => _HomePageBodyState();
}

class _HomePageBodyState extends State<HomePageBody> {
  final ScrollController _scrollController = ScrollController();
  bool notMoreData = false, loading = false;
  int page = 1;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels >=
          _scrollController.position.maxScrollExtent) {
        snackNoMoredata();
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    context.read<CharactersCubit>()
      ..initClient(context: context)
      // ..getEpisodes(1)
      ..getCharacters(page: page);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void snackNoMoredata() {
    if (notMoreData) {
      // String message = 'No hay mas Pets para mostrar por el momento :(';
      // widgetVar!.snackBar(context, message, false);
    } else {
      page++;
      setState(() {
        loading = true;
      });
      context.read<CharactersCubit>().getCharacters(page: page);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      // physics: const NeverScrollableScrollPhysics(),
      controller: _scrollController,
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 25),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            'La serie en números',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              letterSpacing: -1,
              fontSize: 22,
            ),
          ),
        ),
        const SizedBox(height: 15),
        BlocBuilder<CharactersCubit, CharactersState>(
          buildWhen: (previous, current) =>
              current is CharactersLoaded && previous is CharactersLoading,
          builder: (context, state) {
            if (state is CharactersLoaded) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'La serie cuenta actualmente con ${state.episodes} episodios.',
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    letterSpacing: -1,
                    fontSize: 16,
                    
                  ),
                ),
              );
            } else if (state is CharactersLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return Container();
          },
        ),
        const SizedBox(height: 25),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            'Personajes',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
              fontSize: 20,
            ),
          ),
        ),
        const SizedBox(height: 15),
        BlocConsumer<CharactersCubit, CharactersState>(
          listener: (context, state) {
            if (state is CharactersError) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message.toString()),
                ),
              );
            }
          },
          buildWhen: (previous, current) => current is CharactersLoaded,
          builder: (context, state) {
            if (state is CharactersLoaded) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  children: [
                    ListView.builder(
                      // controller: _scrollController,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: state.characters.length,
                      itemBuilder: (BuildContext context, int index) {
                        return CardCharacterWidget(
                          name: state.characters[index].name,
                          image: state.characters[index].image!,
                          species: state.characters[index].species,
                          status: state.characters[index].status,
                          characters: state.characters[index],
                        );
                      },
                    ),
                  ],
                ),
              );
            } else if (state is CharactersLoading) {
              return Column(
                children: const [
                  SizedBox(
                    height: 200,
                  ),
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              );
            }
            return Container();
          },
        )
      ],
    );
  }
}

class CardCharacterWidget extends StatelessWidget {
  const CardCharacterWidget({
    Key? key,
    this.image,
    this.status,
    this.species,
    this.characters,
    this.name,
  }) : super(key: key);
  final String? name;
  final String? image;
  final String? status;
  final String? species;
  final ResultCharacter? characters;

  @override
  Widget build(BuildContext context) {
    double _radius = 25;
    final _size = MediaQuery.of(context).size;
    final Card cardCharacters = Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(_radius),
      ),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(_radius),
              bottomLeft: Radius.circular(_radius),
            ),
            child: FadeInImage(
              placeholder: const AssetImage('assets/img/loading.gif'),
              image: NetworkImage(image!),
              fit: BoxFit.fitHeight,
              width: _size.width * .42,
              height: 170,
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          SizedBox(
            width: _size.width * 0.38,
            height: _size.height * 0.20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Spacer(
                  flex: 5,
                ),
                Text(
                  name!,
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1,
                    fontSize: 20,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                const Spacer(
                  flex: 1,
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      color: status == 'Alive'
                          ? Colors.green
                          : status == 'Dead'
                              ? Colors.red
                              : Colors.grey,
                      size: 13,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      status!,
                      style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                        letterSpacing: -1,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
                const Spacer(
                  flex: 1,
                ),
                Text(
                  species!,
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1,
                    fontSize: 16,
                  ),
                ),
                const Spacer(
                  flex: 5,
                ),
              ],
            ),
          )
        ],
      ),
    );
    return GestureDetector(
      child: cardCharacters,
      onTap: () {
        Navigator.pushNamed(
          context,
          '/detail',
          arguments: characters,
        );
      },
    );
  }
}
