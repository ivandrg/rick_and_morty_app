
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rick_and_morty_app/src/datasources/account_remote_data_source.dart';
import 'package:rick_and_morty_app/src/models/character_model.dart';

import 'package:rick_and_morty_app/src/usecases/get_characters.dart';
import 'package:rick_and_morty_app/src/usecases/get_episodes.dart';

part 'characters_state.dart';

class CharactersCubit extends Cubit<CharactersState> {
  final GetCharactersUseCase? getCharactersUseCase;
  final GetEpisodesUseCase? getEpisodeUseCase;
  final ProfileRemoteDataSource? remoteDataSource;
  CharactersCubit({
    this.getCharactersUseCase,
    this.getEpisodeUseCase,
    this.remoteDataSource,
  }) : super(CharactersLoading());

  // CharactersCubit({
  //   required GetCharactersUseCase? characters,
  // })  : assert(characters != null),
  //       getCharactersUseCase = characters,
  //       super(CharactersLoading());

  Future<void> initClient({required BuildContext? context}) async {
    try {
      await remoteDataSource!.initClientGraphql(context);
    } on NetworkException {
      // emit(CharactersError('Hubo un error al traer los datos'));
    }
  }

  Future<void> getCharacters({required int page}) async {
    try {
      emit(CharactersLoading());
      int episodes = await getEpisodes(1);
      final List<ResultCharacter> characters =
          await getCharactersUseCase!.executeGetCharacters(page);
      emit(CharactersLoaded(characters, episodes));
    } on Failure catch (f) {
      emit(CharactersError(f));
    }
  }

  Future getEpisodes(int page) async {
    try {
      // emit(EpisodesLoading());
      final int episodes = await getEpisodeUseCase!.executeGetEpisodes(1);
      return episodes;
      // emit(EpisodesLoaded(episodes));
    } on Failure catch (f) {
      emit(CharactersError(f));
    }
  }
}
