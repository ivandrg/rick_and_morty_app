part of 'characters_cubit.dart';

@immutable
abstract class CharactersState {}

class CharactersInitial extends CharactersState {}

class CharactersLoading extends CharactersState {}

class EpisodesLoading extends CharactersState {}

class EpisodesLoaded extends CharactersState {
  final int episodes;

  EpisodesLoaded(this.episodes);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is EpisodesLoaded && other.episodes == episodes;
  }

  @override
  int get hashCode => episodes.hashCode;
}

class CharactersError extends CharactersState {
  final Failure message;

  CharactersError(this.message);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CharactersError && other.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}

class CharactersLoaded extends CharactersState {
  final List characters;
  final int episodes;

  CharactersLoaded(this.characters, this.episodes);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CharactersLoaded && other.characters == characters;
  }

  @override
  int get hashCode => characters.hashCode;
}
