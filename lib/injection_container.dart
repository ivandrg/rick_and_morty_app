import 'package:get_it/get_it.dart';
import 'package:rick_and_morty_app/src/cubit/characters_cubit.dart';
import 'package:rick_and_morty_app/src/datasources/account_remote_data_source.dart';
import 'package:rick_and_morty_app/src/usecases/get_characters.dart';
import 'package:rick_and_morty_app/src/usecases/get_episodes.dart';

// This is our global ServiceLocator
final sl = GetIt.instance;

Future<void> init() async {
  //! Features - Number Trivia
  // Bloc
  sl.registerFactory(
    () => CharactersCubit(
      getCharactersUseCase: sl(),
      getEpisodeUseCase: sl(),
      remoteDataSource: sl(),
    ),
  );

  // Use cases
  // Account
  sl.registerLazySingleton(() => GetCharactersUseCase(sl()));
  sl.registerLazySingleton(() => GetEpisodesUseCase(sl()));

  // Repositories
  // sl.registerLazySingleton<AccountRepository>(() => AccountRepositoryImpl());

  // Datasources
  sl.registerLazySingleton<ProfileRemoteDataSource>(
      () => ProfileRemoteDataSourceImpl(listCurrentData: [], listRefetch: []));

  // Entities
  // sl.registerLazySingleton(() => User('', '', '', '', '', ''));

  //!  Core
  // Widgets

  //! External

  // Managers
  // sl.registerLazySingleton<SharedPreferencesManager>(
  // () => TokenMangerImpl(sharedPreferences));
}
