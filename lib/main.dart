import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rick_and_morty_app/src/cubit/characters_cubit.dart';
import 'package:rick_and_morty_app/src/pages/detail_character_page.dart';
import 'package:rick_and_morty_app/src/pages/home_page.dart';
import 'injection_container.dart' as di;

Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarIconBrightness: Brightness.dark,
    ),
  );
  WidgetsFlutterBinding.ensureInitialized();
  await initHiveForFlutter();
  await di.init();
  final HttpLink httpLink = HttpLink(
    'https://rickandmortyapi.com/graphql',
  );
  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      link: httpLink,
      // The default store is the InMemoryStore, which does NOT persist to disk
      cache: GraphQLCache(
        store: HiveStore(),
        // store: store,
      ),
    ),
  );
  runApp(MyApp(
    client: client,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
    this.client,
    // required this.remoteDatasource,
  }) : super(key: key);
  final dynamic client;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => di.sl<CharactersCubit>(),
      child: GraphQLProvider(
        client: client,
        child: MaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const SafeArea(
            child: HomePage(),
          ),
          routes: {
            '/detail': (context) => const DetailCharactesPage(),
          },
        ),
      ),
    );
  }
}
